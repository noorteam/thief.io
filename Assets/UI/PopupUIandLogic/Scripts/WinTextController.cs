﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;

public class WinTextController : MonoBehaviour
{
    [SerializeField] private TextMeshPro _winText;
    [SerializeField] private int _orderInLayer;
    [SerializeField] private List<string> _winTexts;

    private void Awake()
    {
        _winText.renderer.sortingOrder = _orderInLayer;
    }

    [ExposeMethodInEditor]
    public void ShowText()
    {
        _winText.text = _winTexts[Random.Range(0, _winTexts.Count)];
        _winText.enabled = true;
        transform.DOScale(new Vector3(1.3f, 1.3f, 1.3f), 0.5f).SetLoops(2, LoopType.Yoyo);
    }

    public void HideText()
    {
        _winText.enabled = false;
    }
}
