﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public event Action PlayBtnClickedEvent;
    public event Action ReplaBtnClickedEvent;
    public event Action NextLevelBtnClickedEvent;

    [SerializeField] private WinTextController _winTextController;
    [SerializeField] private WinAnimPlayer _winAnimPlayer;
    [SerializeField] private LevelProgressUiController _levelProgressUiController;

    [SerializeField] private GameObject _mainPanel;
    [SerializeField] private GameObject _hudPanel;
    [SerializeField] private GameObject _levelUpPanel;
    [SerializeField] private Button _play;
    [SerializeField] private Button _replay;
    [SerializeField] private Button _next;
    [SerializeField] private Image _levelUpImage;
    [SerializeField] private TextMeshProUGUI _score;
    
    void Start()
    {
        _play.onClick.AddListener(OnPlayBtnClicked);
        _replay.onClick.AddListener(OnReplayBtnClicked);
        _next.onClick.AddListener(OnNextBtnClicked);
        
        ResetProgress();
    }

    public void OnProgressUpdated(float progress)
    {
        _levelProgressUiController.UpdateProgressBar(progress);
        int progressPercent = (int)(progress * 100f);
        _score.text = $"{progressPercent}%";
    }
    
    private void OnPlayBtnClicked()
    {
        _mainPanel.SetActive(false);
        _hudPanel.SetActive(true);
        PlayBtnClickedEvent?.Invoke();
    }

    private void OnReplayBtnClicked()
    {
        ResetProgress();
        ReplaBtnClickedEvent?.Invoke();
    }
    
    private void OnNextBtnClicked()
    {
        ResetProgress();

        _levelUpPanel.SetActive(false);
        _replay.interactable = true;
        NextLevelBtnClickedEvent?.Invoke();
    }

    private void ResetProgress()
    {
        _levelProgressUiController.ResetCurrentProgressSlider();
        _score.text = $"0%";
    }

    public void ShowLevelUp()
    {
        _replay.interactable = false;

        StartCoroutine(PlayWinAnimation());
    }

    public void UpdateCurrentLevelText(int currentLevel)
    {
        _levelProgressUiController.UpdateCurrentLevelText(currentLevel);
    }
    
    private IEnumerator PlayWinAnimation()
    {        
        _winAnimPlayer?.Play(true);
        _winTextController?.ShowText();

        yield return new WaitForSeconds(2f);

        _levelUpPanel.SetActive(true);

        _levelUpImage.color = Color.white;
        _levelUpImage.DOFade(0, 0.7f);
        _winTextController?.HideText();
    }
}
