﻿using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LevelProgressUiController : MonoBehaviour
{
    [SerializeField] private Slider _slider;
    [SerializeField] private TextMeshProUGUI _currentLevel;
    [SerializeField] private TextMeshProUGUI _nextLevel;

    private Tweener _progressBarUpdateTween;

    public void UpdateProgressBar(float endValue)
    {
        if (_progressBarUpdateTween == null)
        {
            _progressBarUpdateTween = DOTween.To(() => _slider.value,
                (t) => { _slider.value = t; },
                endValue, (endValue - _slider.value) * 2);
            _progressBarUpdateTween.SetAutoKill(false);
        }
        else
        {
            _progressBarUpdateTween.ChangeValues(_slider.value, endValue,
                (endValue - _slider.value) * 2);

            _progressBarUpdateTween.Restart();
        }
    }
    
    public void ResetCurrentProgressSlider()
    {
        _progressBarUpdateTween.Pause();
        _slider.value = 0f;
    }
    
    public void UpdateCurrentLevelText(int currentLevelIndex)
    {
        _currentLevel.text = (currentLevelIndex).ToString();
        _nextLevel.text = (currentLevelIndex + 1).ToString();
    }
}
