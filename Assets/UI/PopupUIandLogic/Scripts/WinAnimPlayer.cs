﻿using System.Collections;
using JetBrains.Annotations;
using UnityEngine;

public class WinAnimPlayer : MonoBehaviour
{
    [SerializeField] ParticleSystem[] _threeSources = new ParticleSystem[3];
    [SerializeField] private float _delay;

    [ExposeMethodInEditor]
    public void Play(bool isDelayed = false)
    {
        if (isDelayed)
        {
            StartCoroutine(PlayWithDelay());
        }
        else
        {
            PlayOnEachParticle();
        }
    }

    private void PlayOnEachParticle()
    {
        foreach (var source in _threeSources)
        {
            source.Play(true);
        }
    }

    public IEnumerator PlayWithDelay()
    {
        yield return new WaitForSeconds(_delay);
        PlayOnEachParticle();
    }

    public void Stop()
    {
        foreach (var source in _threeSources)
        {
            source.Stop();
        }
    }
}
