﻿using UnityEngine;

public class PlayerChaser : MonoBehaviour
{
    [SerializeField] private Transform _target;
    
    private LookAtDirectionDelayed _lookAtDirection;
    private PushMovement _pushMovement;
    private SpeedController _speedController;

    private void Awake()
    {
        _lookAtDirection = GetComponent<LookAtDirectionDelayed>();
        _lookAtDirection.SetTarget(_target);
        _pushMovement = GetComponent<PushMovement>();
        _speedController = GetComponent<SpeedController>();
        
    }

    void Update()
    {
        Vector3 dir = _target.position - transform.position;
        dir.y = 0;
        dir.Normalize();

        Vector3 lookAtDirection = _lookAtDirection.CalculateLookAtDirection();
        transform.LookAt(lookAtDirection);

        Vector3 moveDirection = _lookAtDirection.GetMoveDirection();
        moveDirection.y = 0;
        transform.position += (moveDirection * _speedController.CurrentSpeed + _pushMovement.GetCurrentPushVelocity()) * Time.deltaTime;
    } 
}