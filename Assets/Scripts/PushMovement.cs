﻿using UnityEngine;

public class PushMovement : MonoBehaviour
{
    [SerializeField] private AnimationCurve _pushForceByTime;
    [SerializeField] private float _pushEffectDuration;
    [SerializeField] private float _pushFactor = 1f;

    private float _elapsedTimeSincePush;
    private bool _isPushRequested;
    private Vector3 _previousPosition;
    private Vector3 _initialPushVelocity;
    
    public Vector3 Velocity { get; private set;}
    
    private void Update()
    {
        Velocity = (transform.position - _previousPosition) / Time.deltaTime;

        if (_isPushRequested)
        {
            _elapsedTimeSincePush += Time.deltaTime;

            if (_elapsedTimeSincePush > _pushEffectDuration)
            {
                _elapsedTimeSincePush = 0;
                _isPushRequested = false;
            }

            debug = GetCurrentPushVelocity();
        }

        _previousPosition = transform.position;
    }

    public Vector3 debug;

    public Vector3 GetCurrentPushVelocity()
    {
        if (_isPushRequested)
        {
            float t = Mathf.Clamp01(_elapsedTimeSincePush / _pushEffectDuration);

            float forceReduction = _pushForceByTime.Evaluate(t);

            return _initialPushVelocity * forceReduction * _pushFactor;
        }

        return Vector3.zero;
    }

    private void OnCollisionEnter(Collision other)
    {
        if (!_isPushRequested)
        {
            PushMovement otherPushMovement = other.collider.attachedRigidbody.GetComponent<PushMovement>();
            if (otherPushMovement != null)
            {
                Vector3 contactPoint1 = other.GetContact(0).point;
                contactPoint1.y = 0;

                Vector3 delta1 = (transform.position - contactPoint1);

                delta1.Normalize();
                delta1.y = 0;

                Vector3 pushVelocity = delta1 * Vector3.Dot(otherPushMovement.Velocity, delta1);
                OnPush(pushVelocity);

                // DrawDebugLines(contactPoint1, otherPushMovement, pushVelocity);

                otherPushMovement.OnPush(pushVelocity * -1);
            }
        }
    }

    private void DrawDebugLines(Vector3 contactPoint1, PushMovement otherPushMovement, Vector3 pushVelocity)
    {
        Debug.DrawLine(transform.position, contactPoint1, Color.black, 2);
        Debug.DrawLine(otherPushMovement.transform.position,
            otherPushMovement.transform.position + otherPushMovement.Velocity, Color.blue, 2);
        Debug.DrawLine(transform.position, transform.position + Velocity, Color.blue, 2);
        Debug.DrawLine(transform.position, transform.position + pushVelocity, Color.magenta, 2);
        Debug.DrawLine(otherPushMovement.transform.position,
            otherPushMovement.transform.position - pushVelocity, Color.magenta, 2);
    }

    private void OnPush(Vector3 pusherVelocity)
    {
        _isPushRequested = true;
        _elapsedTimeSincePush = 0f;
        _initialPushVelocity = pusherVelocity;
    }
}
