﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;

public class AnimatorController : MonoBehaviour
{
    private Vector3 _velocity = Vector3.zero;
    private bool ShouldFollow { get; set; }
    private Transform _target;

    public void FlyToBox(Transform target)
    {
        _target = target;
        
        ScaleTween();
    }
    
    void Update()
    {
        if (ShouldFollow)
        {
            var targetPosition = _target.position;
            targetPosition.y += 3f;
            
            transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref _velocity, 0.1f);
        }
    }

    private int count;
    private void ScaleTween()
    {
        count++;

        if (count > 2)
        {
            Debug.Log($"{count}", gameObject);    
        }
        
        float time = 0.8f;
        Sequence onComplete = DOTween.Sequence()

            .Append(transform.DOMove(transform.position + new Vector3(0, 3, 3), time/2))
           //.Join(transform.DOScale(transform.localScale * 1.5f, time/2))
            .Join(transform.DORotate(new Vector3(Random.Range(0, 360), Random.Range(0, 360), Random.Range(0, 360)),
                time/2))
            .AppendCallback(()=>
            {
                transform.SetParent(_target, true);
                ShouldFollow = true;
            })
            .Append(transform.DOMoveY(_target.position.y, time/2))
            .Join(transform.DOScale(Vector3.zero, time/2))

            .OnComplete(() =>
            {
                ShouldFollow = false;
                Destroy(gameObject, 0.1f);
            });
    }
}
