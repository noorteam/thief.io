﻿using System;
using UnityEngine;

public class Chaser : MonoBehaviour
{
    [SerializeField] private Transform _target;

    [SerializeField] private float _speed;
    private LookAtDirectionCalculator _lookAtDirection;
    private PushMovement _pushMovement;

    private void Awake()
    {
        _lookAtDirection = GetComponent<LookAtDirectionCalculator>();
        _pushMovement = GetComponent<PushMovement>();
    }

    void Update()
    {
        TargetAndMove();
    }

    private void TargetAndMove()
    {
        Vector3 dir = _target.position - transform.position;
        dir.y = 0;
        dir.Normalize();

        Vector3 lookAtDirection = _lookAtDirection.CalculateLookAtDirection(dir);
        transform.LookAt(lookAtDirection);

        Vector3 moveDirection = _lookAtDirection.GetMoveDirection(dir);
        moveDirection.y = 0;
        transform.position += (moveDirection * ((_speed + ExtraSpedBasedOnDistance(_target.position - transform.position)))
                               + _pushMovement.GetCurrentPushVelocity()) * Time.deltaTime;
    }


    public FloatRange _distanceRange;
    public FloatRange _extraSpeedRange;

    private float ExtraSpedBasedOnDistance(Vector3 distance)
    {
        return _extraSpeedRange.Lerp(_distanceRange.InverseLerp(distance.magnitude));
    }
}
