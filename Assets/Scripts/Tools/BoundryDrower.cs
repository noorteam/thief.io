﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoundryDrower : MonoBehaviour
{
    [SerializeField] private SpriteRenderer _target;
    [SerializeField] private Color _gizmoColor = Color.yellow;
    private Bounds _targetBounds;

    private void Awake()
    {
        InitBounds();
    }

    private void OnValidate()
    {
        InitRenderer();
        InitBounds();
    }

    private void InitRenderer()
    {
        _target = _target ? _target : GetComponent<SpriteRenderer>();
    }

    [ExposeMethodInEditor]
    private void InitBounds()
    {
        _targetBounds = _target.bounds;
    }

    private void Update()
    {
        InitBounds();
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = _gizmoColor;

        Vector3 leftBottom = _targetBounds.center - _targetBounds.extents;
        
        Vector3 leftTop = _targetBounds.center;
        leftTop.x += -_targetBounds.extents.x;     
        leftTop.y += _targetBounds.extents.y;

        Vector3 rightTop = _targetBounds.center + _targetBounds.extents;

        Vector3 rightBottom = _targetBounds.center;
        rightBottom.x += _targetBounds.extents.x;     
        rightBottom.y += - _targetBounds.extents.y;

        Gizmos.DrawLine(leftBottom, leftTop); //Left
        Gizmos.DrawLine(leftTop, rightTop); //Top
        Gizmos.DrawLine(rightTop, rightBottom); //Right
        Gizmos.DrawLine(leftBottom, rightBottom); //Bottom
    }
}
