﻿using UnityEngine;

public class InputHelper
{
    public static Vector3 GetPlaneYIntersection(Camera camera, Transform plane)
    {
        Ray ray = camera.ScreenPointToRay(Input.mousePosition);
        float delta = ray.origin.y - plane.transform.position.y;

        Vector3 dirNormalByY = ray.direction / ray.direction.y;
        Vector3 intersectionPos = ray.origin - dirNormalByY * delta;

        return intersectionPos;
    }
}
