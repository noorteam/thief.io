﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PointerHelper : MonoBehaviour
{
    public static bool IsPointerOverGameObject()
    {
        if (Input.touchCount > 0)
        {
            return EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId);
        }

#if UNITY_EDITOR
        return EventSystem.current.IsPointerOverGameObject();
#endif

        return false;
    }
}
