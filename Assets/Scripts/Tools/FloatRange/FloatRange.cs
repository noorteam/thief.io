﻿using UnityEngine;

[System.Serializable]
public struct FloatRange {

    [SerializeField]
    float min, max;

    public float Min
    {
        get => min;
        set => min = value;
    }

    public float Max => max;

    public float RandomValueInRange => Random.Range(min, max);

    public FloatRange (float value) 
    {
        min = max = value;
    }

    public FloatRange (float min, float max) {
        this.min = min;
        this.max = max < min ? min : max;
    }

    public float Lerp(float t)
    {
        return Mathf.Lerp(min, max, t);
    }
	
    public float InverseLerp(float value)
    {
        return Mathf.InverseLerp(min, max, value);
    }

    public bool IsInRange(float value)
    {
        return value >= min && value <= max;
    }

    public float Clamp(float value)
    {
        return Mathf.Clamp(value, min, max);
    }
}