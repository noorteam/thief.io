﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class MeshInsideOutConverter : MonoBehaviour
{
    [ExposeMethodInEditor]
    void Convert()
    {
        MeshFilter filter = GetComponent<MeshFilter>();

        Mesh mesh = filter.mesh;
        Vector3[] normals = mesh.normals;

        for (int i = 0; i < normals.Length; i++)
        {
            normals[i] *= -1;
        }

        mesh.normals = normals;
        mesh.triangles = mesh.triangles.Reverse().ToArray();

        AssetDatabase.CreateAsset(mesh, "Assets/Models/obj.asset");
        AssetDatabase.SaveAssets();
    }
}
#endif
