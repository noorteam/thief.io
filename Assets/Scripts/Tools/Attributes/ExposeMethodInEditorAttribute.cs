// The MIT License (MIT) - https://gist.github.com/bcatcho/1926794b7fd6491159e7
// Copyright (c) 2015 Brandon Catcho
using System;

// Restrict to methods only
[AttributeUsage(AttributeTargets.Method)]
public class ExposeMethodInEditorAttribute : Attribute
{
    private bool _isValueChange;
    private bool _isForceExecuteInEditMode;

    public bool IsForceExecuteInEditMode => _isForceExecuteInEditMode;

    public bool IsValueChange => _isValueChange;

    public ExposeMethodInEditorAttribute(bool isValueChange = false, bool isForceExecuteInEditMode = false)
    {
        _isValueChange = isValueChange;
        _isForceExecuteInEditMode = isForceExecuteInEditMode;
    }
}