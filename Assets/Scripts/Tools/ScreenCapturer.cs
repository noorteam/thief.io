﻿using System.Collections;
using UnityEngine;

public class ScreenCapturer : MonoBehaviour
{
	public bool shoot = false;
	public string prefix;


	void Update () 
	{
		if (shoot)
		{
			shoot = false;

			StartCoroutine(CaptureScreenshot());
		}
	}

	private IEnumerator  CaptureScreenshot()
	{
		yield return new WaitForEndOfFrame();
		
		ScreenCapture.CaptureScreenshot("ScreenShots/" + prefix + "_" + Screen.width + "x" + Screen.height + ".png");
	}
}
