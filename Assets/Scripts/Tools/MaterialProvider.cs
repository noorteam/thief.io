﻿using UnityEngine;
using UnityEngine.Assertions;

public class MaterialProvider : MonoBehaviour
{
    [SerializeField] private Material _pieceDefaultMaterial;
    [SerializeField] private Material _pieceBurnedMaterial;
    [SerializeField] private Material _pieceMissedMaterial;
    
    private static MaterialProvider _instance;

    public static Material PieceDefaultMaterial => _instance._pieceDefaultMaterial;
    public static Material PieceBurnedMaterial => _instance._pieceBurnedMaterial;
    public static Material PieceMissedMaterial => _instance._pieceMissedMaterial;

    public static MaterialProvider Instance => _instance;

    private void Awake()
    {
        Assert.IsTrue(_instance == null, "This class should have only One instance");

        _instance = this;
    }
}
