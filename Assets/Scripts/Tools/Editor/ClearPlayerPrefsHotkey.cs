﻿using UnityEditor;
using UnityEngine;

public class ClearPlayerPrefsHotkey
{
    [MenuItem("Tools/Clear PlayerPrefs &c")] // Alt + c
    private static void NewMenuOption()
    {
        Debug.Log("PlayerPrefs.DeleteAll()");
        PlayerPrefs.DeleteAll();
    }
}
