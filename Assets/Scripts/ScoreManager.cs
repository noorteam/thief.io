﻿using System;
using System.Collections;
using System.Diagnostics.Tracing;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.Assertions;
using Random = UnityEngine.Random;

public class ScoreManager : MonoBehaviour
{
    public event Action ScoreUpdatedEvent;

    private static string CollectableTag = "Collectable";
    private static string PlayerTag = "Player";

    [SerializeField] private CollectibleGenerator _collectibleGenerator;
    [SerializeField] private Transform _box;
    [SerializeField] private TextMeshPro _score;
    [SerializeField] private int _initialScoreCount;
    private int _collectedCount = 0;
    private int _dropPerPlayerCollision = 1;

    public int CollectedCount => _collectedCount;

    private void Start()
    {
        AddCollectibles(_initialScoreCount);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(CollectableTag))
        {
            _box.gameObject.SetActive(true);
            
            Collected(other);

            AddCollectibles();
        }
        else if (other.CompareTag(PlayerTag))
        {
            ProcessCollectibleDropWithOtherPlayer(other);
        }
    }

    private void Collected(Collider other)
    {
        other.enabled = false;
        other.GetComponent<AnimatorController>().FlyToBox(_box);
    }

    private void AddCollectibles(int count = 1)
    {
        _collectedCount += count;
        _score.text = _collectedCount.ToString();
        ScoreUpdatedEvent?.Invoke();
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.collider.CompareTag("OldMan"))
        {
            DecreaseCollectibles(_collectedCount);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag(PlayerTag))
        {
            ProcessCollectibleDropWithOtherPlayer(other);
        }
    }

    private void ProcessCollectibleDropWithOtherPlayer(Collider other)
    {
        ScoreManager otherScoreManager = other.attachedRigidbody.GetComponent<ScoreManager>();
        Assert.IsTrue(otherScoreManager != null);

        if (otherScoreManager.CollectedCount > _collectedCount)
        {
            DecreaseCollectibles(_dropPerPlayerCollision);
        }
    }

    private void DecreaseCollectibles(int requestedDropAmount)
    {
        if (_collectedCount > 0)
        {
            _box.gameObject.SetActive(false);

            int actualDropAmount = Mathf.Min(requestedDropAmount, _collectedCount); 
           _collectedCount -= actualDropAmount;
            _score.text = _collectedCount.ToString();

            GenerateCollectibles(actualDropAmount);
            ScoreUpdatedEvent?.Invoke();
        }
    }

    private void GenerateCollectibles(int actualDropAmount)
    {
        for (int i = 0; i < actualDropAmount; i++)
        {
            Vector3 randomShift = (Random.insideUnitCircle).normalized * Random.Range(3f, 30f);
            
            Vector3 collectiblePosition = transform.position + new Vector3(randomShift.x, randomShift.z, randomShift.y);
            collectiblePosition.y = 0;

            GameObject collectible =  _collectibleGenerator.CreateCollectible(collectiblePosition);
            // collectible.SetActive(false);
            //
            // StartCoroutine(EnableCollectible(collectible));
        }
    }

    private IEnumerator EnableCollectible(GameObject collectible)
    {
        yield return new WaitForSeconds(Random.Range(0.1f, 0.5f));
        collectible.SetActive(true);
    }
}
