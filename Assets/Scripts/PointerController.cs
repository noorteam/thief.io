﻿using UnityEngine;

public class PointerController : MonoBehaviour
{
    [SerializeField] private Vector3 _offset;
    [SerializeField] private float _shiftFactor = 0.5f;
    private Camera _camera;

    private void Awake()
    {
        _camera = Camera.main;
    }

    private void Update()
    {
        if (Input.GetMouseButton(0))
        {
            transform.localPosition = _offset + GetMousePosition() * _shiftFactor;
        }
    }

    private Vector3 GetMousePosition()
    {
        Vector3 mousePositionInScreenSpace = Input.mousePosition;
        Vector3 position = new Vector3(mousePositionInScreenSpace.x, mousePositionInScreenSpace.y, 20);
        Vector3 screenToWorldPoint = _camera.ScreenToWorldPoint(position);
        Vector3 mousePositionInCameraSpace = _camera.transform.InverseTransformPoint(screenToWorldPoint);

        return mousePositionInCameraSpace;
    }
}
