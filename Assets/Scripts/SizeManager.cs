﻿using DG.Tweening;
using Tayx.Graphy.Utils;
using UnityEngine;

public class SizeManager : MonoBehaviour
{
    [SerializeField] private float _deltaScalePerCollectible = 0.01f;
    [SerializeField] private float _maxScale = 2f;

    private ScoreManager _scoreManager;
    private Tweener _scaleAnim;
    private float _scaleFactor;

    private void Awake()
    {
        _scoreManager = GetComponent<ScoreManager>();
        _scoreManager.ScoreUpdatedEvent += OnScoreUpdated;
    }

    private void OnScoreUpdated()
    {
        _scaleAnim?.Kill();
        _scaleFactor = Mathf.Min(1 + _deltaScalePerCollectible * _scoreManager.CollectedCount, _maxScale);
        Vector3 scale = Vector3.one * _scaleFactor;
        _scaleAnim = transform.DOScale(scale, 0.5f); 
    }

    public float Scale()
    {
        return _scaleFactor;
    }
}
