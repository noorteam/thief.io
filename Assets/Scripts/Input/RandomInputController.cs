﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class RandomInputController : AbstractInputController
{
    [SerializeField] private float _maxWalkAwayArea;
    [SerializeField] private float _inidialDelay;
    private bool _shouldUpdate;
    private FloatRange _changeInputAfterRange = new FloatRange(0.5f, 2f);
    private float _currentDelay;
    private float _elapsedTime;
    private Vector3 _previousRandomInput;

    private void Awake()
    {
        FistTapInput();
        // Invoke(nameof(FistTapInput), Random.Range(0.5f, 3f));
    }

    private void FistTapInput()
    {
        _previousRandomInput = transform.position;
        OnTap(_previousRandomInput);
        OnMove(_previousRandomInput + transform.forward * 5f);
        _currentDelay = _inidialDelay;
        _shouldUpdate = true;
    }

    public void Update()
    {
        if (_shouldUpdate)
        {
            if (_elapsedTime > _currentDelay)
            {
                _currentDelay = _changeInputAfterRange.RandomValueInRange;
                _elapsedTime = 0;

                Vector3 randomInput = GetInputDirection();
                randomInput.y = 0;
                randomInput += _previousRandomInput;

                randomInput = randomInput.normalized * 5f;

                OnMove(randomInput);

                _previousRandomInput = randomInput;
            }

            _elapsedTime += Time.deltaTime;
        }
    }

    private  Vector3 GetInputDirection()
    {
        return transform.position.magnitude > _maxWalkAwayArea ? -transform.position : Random.insideUnitSphere;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(Vector3.zero, _maxWalkAwayArea);
    }
}
