﻿using System;
using UnityEngine;

public class AbstractInputController : MonoBehaviour
{
    public event Action<Vector3> MouseTapEvent;
    public event Action<Vector3> MouseMoveEvent;
    public event Action<Vector3> MouseReleaseEvent;

    protected void OnTap(Vector3 position)
    {
        MouseTapEvent?.Invoke(position);
    }

    protected void OnMove(Vector3 position)
    {
        MouseMoveEvent?.Invoke(position);
    }

    protected void OnRelease(Vector3 position)
    {
        MouseReleaseEvent?.Invoke(position);
    }
}
