﻿using System;
using UnityEngine;

public class MouseInputController : AbstractInputController
{
    private Camera _camera;

    private void Awake()
    {
        _camera = Camera.main;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            OnTap(GetMousePosition());
        }
        else if (Input.GetMouseButton(0))
        {
           OnMove(GetMousePosition());
        }
        else if (Input.GetMouseButtonUp(0))
        {
            OnRelease(GetMousePosition());
        }
    }

    private Vector3 GetMousePosition()
    {
        Vector3 mousePositionInScreenSpace = Input.mousePosition;
        Vector3 position = new Vector3(mousePositionInScreenSpace.x, mousePositionInScreenSpace.y, 20);
        Vector3 screenToWorldPoint = _camera.ScreenToWorldPoint(position);
        Vector3 mousePositionInCameraSpace = _camera.transform.InverseTransformPoint(screenToWorldPoint);
    
        Vector3 mouseRelativePosition = new Vector3()
        {
            x = mousePositionInCameraSpace.x,
            y = mousePositionInCameraSpace.z - _camera.transform.position.y,
            z = mousePositionInCameraSpace.y
        };

        return mouseRelativePosition;
    }
}
