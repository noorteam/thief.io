﻿using System;
using UnityEngine;

public class CameraZoom : MonoBehaviour
{
    [SerializeField] private Transform _target;
    [SerializeField] private float _zoomFactorPerScale = 0;
    private Vector3 _initialLocalPosition;
    private Vector3 _zoomOutVelocity;

    [SerializeField] private float _delay;
    private float _previousScaleX;
    private float _elapsedTime;
    private bool _isTimerStarted;

    private void Awake()
    {
        _initialLocalPosition = transform.localPosition;
    }

    private void Update()
    {
        // if (Mathf.Abs(_previousScaleX - targetScale) > 0.1f && !_isTimerStarted)
        // {
        //     _isTimerStarted = true;
        // }
        //
        // if (_isTimerStarted)
        // {
        //     _elapsedTime += Time.deltaTime;
        // }
        //
        // if (_elapsedTime > _delay)
        // {
        //     _isTimerStarted = false;
        //     _previousScaleX = targetScale;
        //     _elapsedTime = 0;
        // }
        //
        // // if (!_isTimerStarted)
        // {
           float targetScale = _target.localScale.x;
            Vector3 targetPosition =
                _initialLocalPosition + Vector3.forward * ((1 - targetScale) * _zoomFactorPerScale);
            
            //transform.localPosition = Vector3.SmoothDamp(transform.localPosition, targetPosition, ref _zoomOutVelocity, 0.8f);
            transform.localPosition = Vector3.Lerp(transform.localPosition, targetPosition,0.01f);
        // }
    }
}
