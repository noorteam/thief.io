﻿using DG.Tweening;
using UnityEditor;
using UnityEngine;

public class ScoreFollower : MonoBehaviour
{
    [SerializeField] private float _yOffset = 10f;
    [SerializeField] private Transform _target;

    private SizeManager _sizeManager;

    private void Start()
    {
        _sizeManager = _target.GetComponent<SizeManager>();
    }

    private void Update()
    {
        Vector3 infoPosition = new Vector3()
        {
            x = _target.position.x,
            y = _target.position.y + _sizeManager.Scale() * 4.5f,
            z = _target.position.z
        };

        transform.position = Vector3.Lerp(transform.position, infoPosition, 0.5f);
        // transform.DOScale(_sizeManager.Scale(), 0f);

        transform.localScale = Vector3.one * _sizeManager.Scale();
    }
}
