﻿using DG.Tweening;
using UnityEngine;

public class CollectibleGenerator : MonoBehaviour
{
    [SerializeField] private Vector3 _offset;
    [SerializeField] private GameObject _collectiblePrefab;
    [SerializeField] private Transform _collectibleContainer;

    public GameObject CreateCollectible(Vector3 position)
    {
        GameObject collectible = Instantiate(_collectiblePrefab, _collectibleContainer, true);
        collectible.transform.position = position + _offset;
        collectible.transform.localScale = Vector3.zero;
        collectible.transform.DOScale(Vector3.one, Random.Range(0.2f, 2f)).SetDelay(Random.Range(.1f, .5f));

        return collectible;
    }
}
