﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField, FloatRangeSlider(0, 360)] private FloatRange _moveAngleRange;
    private AbstractInputController _inputController;
    private LookAtDirectionCalculator _lookAtDirectionCalculator;
    private Vector3 _playerDirection;
    private Vector3 _mousePreviousPosition;
    private Vector3 _rotationVelocity;
    private float _maxMouseDeltaMove = 1.1f;
    private bool _shouldRotate;
    private Vector3 _previousMousePositionDiff;
    private ScoreManager _scoreManager;
    private SpeedController _speedController;

    private void Awake()
    {
        _lookAtDirectionCalculator = GetComponent<LookAtDirectionCalculator>();
        
        _inputController = GetComponent<AbstractInputController>();
        _inputController.MouseTapEvent += OnTap;
        _inputController.MouseMoveEvent += OnMove;
        _inputController.MouseReleaseEvent += OnRelease;

        _scoreManager = GetComponent<ScoreManager>();
        _scoreManager.ScoreUpdatedEvent += OnScoreUpdated;

        _speedController = GetComponent<SpeedController>();
    }

    private void OnScoreUpdated()
    {
        UpdateMoveAngle();
    }

    private void OnTap(Vector3 position)
    {
        _mousePreviousPosition = position;
        _shouldRotate = true;
    }

    private void OnMove(Vector3 position)
    {
        Vector3 currentMousePosition = position;
        Vector3 mousePositionDiff = currentMousePosition - _mousePreviousPosition;
        mousePositionDiff.y = 0;
        mousePositionDiff += _previousMousePositionDiff;

        if (mousePositionDiff.magnitude > _maxMouseDeltaMove)
        {
            mousePositionDiff = mousePositionDiff.normalized * _maxMouseDeltaMove;
        }

        _mousePreviousPosition = currentMousePosition;
        _playerDirection = mousePositionDiff.normalized * _maxMouseDeltaMove;
        
        _previousMousePositionDiff = mousePositionDiff;
    }

    private void OnRelease(Vector3 position)
    {
        _shouldRotate = false;
    }

    private void Update()
    {
        RotateTransform();
        MoveTransform();
    }

    private void RotateTransform()
    {
        if (_shouldRotate)
        {
            Vector3 direction = _playerDirection;
            Vector3 lookAtDirection = _lookAtDirectionCalculator.CalculateLookAtDirection(direction);
            transform.LookAt(lookAtDirection);
        }
    }

    private void MoveTransform()
    {
        transform.position += transform.forward * (_speedController.CurrentSpeed * Time.deltaTime);
    }

    private void UpdateMoveAngle()
    {
        float speedFactor = _speedController.GetSpeedFactor();
        _lookAtDirectionCalculator.MaxAngleToRotate = _moveAngleRange.Lerp(1f - speedFactor);
    }
}
