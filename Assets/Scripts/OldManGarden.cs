﻿using System;
using UnityEngine;

public class OldManGarden : MonoBehaviour
{
    public event Action<Transform> PlayerThiefEvent;

    private Transform _playerInRegion;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            ScoreManager scoreManager = other.attachedRigidbody.GetComponent<ScoreManager>();
            scoreManager.ScoreUpdatedEvent += OnScoreUpdatedEvent;
            _playerInRegion = scoreManager.transform;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            StopListeningToPlayer(other.attachedRigidbody.transform);
        }
    }

    private void StopListeningToPlayer(Transform playerTransform)
    {
        ScoreManager scoreManager = playerTransform.GetComponent<ScoreManager>();
        scoreManager.ScoreUpdatedEvent -= OnScoreUpdatedEvent;
        _playerInRegion = null;
    }

    private void OnScoreUpdatedEvent()
    {
        if (_playerInRegion != null)
        {
            gameObject.SetActive(false);
            PlayerThiefEvent?.Invoke(_playerInRegion);
            StopListeningToPlayer(_playerInRegion);
        }
    }
}
