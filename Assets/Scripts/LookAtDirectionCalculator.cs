﻿using UnityEngine;

public class LookAtDirectionCalculator : MonoBehaviour
{
    [SerializeField] private float _maxAngleToRotate = 90f;
    [SerializeField] private float _lerpFactor = 3f;
    private Vector3 _rotationVelocity;

    public float MaxAngleToRotate
    {
        set { _maxAngleToRotate = value; }
    }

    public Vector3 CalculateLookAtDirection(Vector3 direction)
    {
        Vector3 moveDirection = GetMoveDirection(direction);

        Vector3 lookAtDirection =
            Vector3.SmoothDamp(transform.position + transform.forward, transform.position + moveDirection,
                ref _rotationVelocity, 0.05f);

        return lookAtDirection;
    }

    public Vector3 GetMoveDirection(Vector3 direction)
    {
        float angleToTarget = Vector3.Angle(transform.forward, direction);
        Vector3 moveDirection;

        if (angleToTarget > _maxAngleToRotate)
        {
            moveDirection = Vector3.Slerp(transform.forward, direction, 1f / _lerpFactor);
        }
        else
        {
            moveDirection = direction;
        }

        return moveDirection;
    }
}
