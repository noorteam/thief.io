﻿using System.Collections;
using UnityEngine;

public class Chasing : MonoBehaviour
{
    private static string PlayerTag = "Player";

    [SerializeField] private OldManGarden _garden;
    [SerializeField] private Transform _target;
    [SerializeField] private bool _isChasing;
    [SerializeField] private bool _isAngry;
    [SerializeField] private OldManAnimatorController _animController;
    [SerializeField, ReadonlyField] private float _speed = 3f;
    [SerializeField] private float _extraSpeed;

    [SerializeField, FloatRangeSlider(1, 2)] private FloatRange _speedFactorRange;
    [SerializeField, FloatRangeSlider(1, 20)] private FloatRange _distancRange;

    private LookAtDirectionDelayed _lookAtDirectionDelayed;
    private float _minDistanceToChase = 1f;
    private Vector3 _rotationVelocity;
    private PushMovement _pushMovement;
    private SpeedController _targetSpeedController;
    [SerializeField, ReadonlyField] private float _speedFactorBasedOnRange;

    private void Awake()
    {
        _lookAtDirectionDelayed = GetComponent<LookAtDirectionDelayed>();
        _garden.PlayerThiefEvent += OnPlayerThief;

        _pushMovement = GetComponent<PushMovement>();
    }

    private void OnPlayerThief(Transform playerTransform)
    {
        if (_targetSpeedController != null)
        {
            _targetSpeedController.SpeedUpdatedEvent -= OnTargetSpeedUpdated;
        }

        _target = playerTransform;
        _targetSpeedController = _target.GetComponent<SpeedController>();
        _targetSpeedController.SpeedUpdatedEvent += OnTargetSpeedUpdated;
        OnTargetSpeedUpdated(_targetSpeedController.CurrentSpeed);


        _animController.Run();
        StartCoroutine(StartChasing(_target));

        _isAngry = true;

        _lookAtDirectionDelayed.SetTarget(_target);
        transform.LookAt(_target);
    }

    private void OnTargetSpeedUpdated(float targetSpeed)
    {
        _speed = targetSpeed + _extraSpeed;
    }

    void Update()
    {
        if (_isChasing && _isAngry)
        {
            Vector3 distanceBetweenTarget = _target.position - transform.position;
            if (distanceBetweenTarget.magnitude < _minDistanceToChase)
            {
                _isChasing = false;
                _animController.Stop();
            }
            else
            {
                TargetAndMove();
            }
        }
    }

    
    private void TargetAndMove()
    {
        float distanceFactor = _distancRange.InverseLerp((transform.position - _target.position).magnitude);
        _speedFactorBasedOnRange = _speedFactorRange.Lerp(distanceFactor);

        Vector3 lookAtDirection = _lookAtDirectionDelayed.CalculateLookAtDirection();
        transform.LookAt(lookAtDirection);

        Vector3 moveDirection = _lookAtDirectionDelayed.GetMoveDirection();
        moveDirection.y = 0;
        transform.position += (moveDirection * _speed * _speedFactorBasedOnRange + _pushMovement.GetCurrentPushVelocity()) * Time.deltaTime;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!_isChasing && _isAngry)
        {
            CheckForChasing(other);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (!_isChasing && _isAngry)
        {
            CheckForChasing(other);
        }
    }

    private void CheckForChasing(Collider other)
    {
        if ( other.CompareTag(PlayerTag) && other.transform != _target)
        {
            ScoreManager scoreManager = other.attachedRigidbody.GetComponent<ScoreManager>();
            if (scoreManager.CollectedCount > 0)
            {
                _animController.Run();
                StartCoroutine(StartChasing(other.attachedRigidbody.transform));
            }
        }
    }

    private IEnumerator StartChasing(Transform otherTransform)
    {
         yield return new WaitForSeconds(.4f);

         _isChasing = true;
        _target = otherTransform;
    }
}
