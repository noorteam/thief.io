﻿using DG.Tweening;
using UnityEngine;

public class OldManAnimatorController : MonoBehaviour
{
    [SerializeField] private Animator _oldManAnim;
    [SerializeField] private Vector3 _movePos;

    private Sequence _idle;
    private Sequence _run;
    private Tween _moveTween;


    private void Start()
    {
        _moveTween = transform.DOMove(_movePos, 8f).Pause();
        IdleAnim();
    }

    private void IdleAnim()
    {
        _idle = DOTween.Sequence()
            .AppendCallback(() =>
                {
                    _moveTween.Restart();
                })
            .AppendInterval(6f)
            .AppendCallback(() =>
                {
                    _oldManAnim.SetBool("IsRunning", false);
                    _oldManAnim.SetBool("IsIdle", true);
                })
            .AppendInterval(7)
            .AppendCallback(() =>
                {
                    _oldManAnim.SetBool("IsRunning", false);
                    _oldManAnim.SetBool("IsIdle", false);
                })
            .AppendCallback(() =>
                {
                   var eulerAngles = transform.localEulerAngles;
                    eulerAngles.y *= -1;
                   transform.localEulerAngles = eulerAngles;
                   
                   _moveTween.Flip();
                })
            .SetLoops(-1, LoopType.Restart);
    }

    [ExposeMethodInEditor]
    public void Run()
    {
        _idle.Kill();
        DOTween.Kill(transform);
        _oldManAnim.SetBool("IsRunning", true);
        _oldManAnim.SetBool("IsIdle", false);
    }

    [ExposeMethodInEditor]
    public void Stop()
    {
        _run = DOTween.Sequence();
        _run.AppendCallback(() =>
            {
                _oldManAnim.SetBool("IsRunning", false);
                _oldManAnim.SetBool("IsIdle", false);
            })
            .AppendCallback(IdleAnim);
    }
}
