﻿using System;
using UnityEngine;

public class SpeedController : MonoBehaviour
{
    public event Action<float> SpeedUpdatedEvent;

    [SerializeField] private float _initialSpeed = 3;
    [SerializeField, FloatRangeSlider(0, 15)] private FloatRange _speedRange;
    [SerializeField] private float _deltaSpeedPerCollectible = 0.1F;
    [SerializeField, ReadonlyField] private float _speed;
    [SerializeField] private Animator _animator;
    private ScoreManager _scoreManager;
    private float _currentSpeed;
    private AbstractInputController _inputController;

    public float CurrentSpeed
    {
        get => _currentSpeed;
    }

    private void Awake()
    {
        _scoreManager = GetComponent<ScoreManager>();
        _scoreManager.ScoreUpdatedEvent += OnScoreUpdated;

        if (_inputController != null)
        {
            _inputController = GetComponent<AbstractInputController>();
            _inputController.MouseTapEvent += OnTap;
        }

        _speed = _initialSpeed;
    }

    private void OnTap(Vector3 obj)
    {
        _currentSpeed = _speed;
    }

    private void OnScoreUpdated()
    {
        _speed = _speedRange.Clamp(_initialSpeed + _deltaSpeedPerCollectible * _scoreManager.CollectedCount);
        _currentSpeed = _speed;

        _animator.speed = 1.1f + (.8f - 1.1f) * ((_speed - _initialSpeed) / (_speedRange.Max - _initialSpeed));

        SpeedUpdatedEvent?.Invoke(_currentSpeed);
    }

    public float GetSpeedFactor()
    {
        return _speedRange.InverseLerp(_speed);
    }
}
