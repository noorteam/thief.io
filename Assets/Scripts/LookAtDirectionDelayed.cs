﻿using UnityEngine;

public class LookAtDirectionDelayed : MonoBehaviour
{
    [SerializeField] private float _lerpFactor = 3f;
    [SerializeField] private float _delay = 0.3f;
    private Transform _target;
    private Vector3 _targetPreviousPosition;
    private Vector3 _targetPreviousDirection;
    private Vector3 _aimPosition;
    private Vector3 _currentAimPosition;
    private Vector3 _aimSmoothVelocity;
    private Vector3 _rotationVelocity;
    private float _elapsedTime;

    public Vector3 CalculateLookAtDirection()
    {
        Vector3 moveDirection = GetMoveDirection();

        Vector3 lookAtDirection =
            Vector3.SmoothDamp(transform.position + transform.forward, transform.position + moveDirection,
                ref _rotationVelocity, 0.05f);

        return lookAtDirection;
    }


    public Vector3 GetMoveDirection()
    {
        Vector3 moveDirection = Vector3.Slerp(transform.forward, GetDirection(), 1f / _lerpFactor);

        return moveDirection;
    }

    public void SetTarget(Transform target)
    {
        _target = target;
        _aimPosition = _target.position;
    }

    private void Update()
    {
        if (_target != null)
        {
            if (_elapsedTime > _delay)
            {
                _elapsedTime = 0;
                _aimPosition = _targetPreviousPosition + _targetPreviousDirection * 7f;
                _targetPreviousPosition = _target.position;
                _targetPreviousDirection = _target.forward;
            }

            _currentAimPosition = Vector3.SmoothDamp(_currentAimPosition, _aimPosition, ref _aimSmoothVelocity, 0.1f);
            _elapsedTime += Time.deltaTime;
        }
    }

    private Vector3 GetDirection()
    {
        return (_currentAimPosition - transform.position).normalized;
    }
}
