﻿using System;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform _target;
    public Vector3 _offset;
    private Vector3 _velocity;
    public bool ShouldFollow { get; set; }

    private void Awake()
    {
        ShouldFollow = true;
    }

    void Update()
    {
        if (ShouldFollow)
        {
            Vector3 transformPosition = transform.position;
            transformPosition = _target.position + _offset;

            transform.position = Vector3.SmoothDamp(transform.position, transformPosition, ref _velocity, 0.2f);
        }
    }
}
