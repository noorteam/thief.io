﻿using UnityEngine;

public class PlayerMovementNew : MonoBehaviour
{
     [SerializeField] private float _arrowMaxDistance;
     [SerializeField] private Transform _arrowHolder;
     private Transform _arrow;
     private Vector3 _arrowVector;
     private Vector3 _direction;
     private LookAtDirectionCalculator _lookAtDirection;
     private AbstractInputController _inputController;
     private SpeedController _speedController;
     private PushMovement _pushMovement;

     private Vector3 _basePosition;
     private Vector3 _currentPosition;

     private void Awake()
    {
        _direction = Vector3.forward;
        _arrowVector = _arrowMaxDistance * Vector3.forward;
        _direction = Vector3.forward;

        if (_arrowHolder != null)
        {
            _arrow = _arrowHolder.GetChild(0);
        }

        _lookAtDirection = GetComponent<LookAtDirectionCalculator>();
        _pushMovement = GetComponent<PushMovement>();

        _speedController = GetComponent<SpeedController>();

        _inputController = GetComponent<AbstractInputController>();
        _inputController.MouseTapEvent += OnTap;
        _inputController.MouseMoveEvent += OnMove;
        _inputController.MouseReleaseEvent += OnRelease;
    }

    private void Update()
    {
        UpdateDirection();
        Move();
        Rotate();
        UpdateArrow();
    }

    private void UpdateDirection()
    {
        _direction += _arrowVector;
        _direction.Normalize();
    }

    private void Move()
    {
        transform.position += (transform.forward * _speedController.CurrentSpeed + _pushMovement.GetCurrentPushVelocity()) * Time.deltaTime;
    }

    private void Rotate()
    {
         Vector3 lookAtDirection = _lookAtDirection.CalculateLookAtDirection(_direction);
         transform.LookAt(lookAtDirection);
    }

    private void OnRelease(Vector3 getMousePosition) { }

    private void OnMove(Vector3 getMousePosition)
    {
        _currentPosition = getMousePosition;

        _arrowVector = _currentPosition - _basePosition;
        _arrowVector.y = 0;
        Debug.DrawRay(_basePosition, _arrowVector, Color.cyan);

        if (_arrowVector.magnitude > _arrowMaxDistance)
        {
            CalculateTapPosition();
        }
    }

    private void UpdateArrow()
    {
        if (_arrowHolder != null)
        {
            _arrowHolder.LookAt(_arrowHolder.position + _arrowVector, Vector3.up);

            float arrowDistance = Mathf.Min(_arrowMaxDistance, _arrowVector.magnitude);

            _arrow.transform.localPosition = Vector3.up * 0.1f + Vector3.forward * arrowDistance;
            _arrowHolder.position = transform.position;
        }
    }

    private void OnTap(Vector3 getMousePosition)
    {
        _currentPosition = getMousePosition;
        _arrowVector = _arrowVector.normalized * _arrowMaxDistance;

        CalculateTapPosition();
    }

    private void CalculateTapPosition()
    {
        _basePosition = _currentPosition - _arrowVector.normalized * _arrowMaxDistance;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere( _basePosition, 0.1f);

        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(_currentPosition, 0.1f);
    }
}
